//
//  ViewController.swift
//  LocalizationAppTest
//
//  Created by Rafael Montilla on 1/15/19.
//  Copyright © 2019 RM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var changeLanguageButton: UIButton!
    @IBOutlet var reloadButton: UIButton!
    @IBOutlet var buyButton: UIButton!
    @IBOutlet var helloWorldLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    //MARK: - Private methods
    func reloadLocalizationData() {
        APIHelper.shared.fetchLocalizationData { (success) in
            if (success){
                self.refreshStrings()
            } else {
                print("error")
            }
        }
    }
    
    func refreshStrings() {
        
        DispatchQueue.main.async {
            self.buyButton.setTitle(LanguageHelper.shared.localizeString("buy"), for: .normal)
            self.changeLanguageButton.setTitle(LanguageHelper.shared.localizeString("changeLanguage"), for: .normal)
            self.reloadButton.setTitle(LanguageHelper.shared.localizeString("reload"), for: .normal)
            self.helloWorldLabel.text = LanguageHelper.shared.localizeString("helloWorld")
        }
    }
    
    
    func showLanguageSelection() {
        
        let actionSheet = UIAlertController(title: "Choose Language", message: nil, preferredStyle: .actionSheet)
        
        let enAction = UIAlertAction(title: "English", style: .default) { (action) in
            LanguageHelper.shared.setLanguage("en")
            self.refreshStrings()
        }
        let esAction = UIAlertAction(title: "Spanish", style: .default) { (action) in
            LanguageHelper.shared.setLanguage("es")
            self.refreshStrings()
        }
        let jaAction = UIAlertAction(title: "Japanese", style: .default) { (action) in
            LanguageHelper.shared.setLanguage("ja")
            self.refreshStrings()
        }
        
        actionSheet.addAction(enAction)
        actionSheet.addAction(esAction)
        actionSheet.addAction(jaAction)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: - Action methods
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        
        switch sender {
        case changeLanguageButton:
            showLanguageSelection()
            break
        case reloadButton:
            reloadLocalizationData()
            break
        case buyButton:
            buyButtonTapped()
            break
        default:
            break
        }
    }
    
    func buyButtonTapped(){
    }
    
}

