//
//  LanguageHelper.swift
//  LocalizationAppTest
//
//  Created by Rafael Montilla on 1/15/19.
//  Copyright © 2019 RM. All rights reserved.
//

import UIKit

class LanguageHelper {
    static let shared = LanguageHelper()
    var bundle : Bundle?
    
    init() {
        
       let languages = UserDefaults.standard.array(forKey: "AppleLanguages") as! [String];
       let current = languages[0]
       self.setLanguage(current)
    }
    
    func localizeString(_ value : String) -> String{
        return bundle!.localizedString(forKey: value, value: nil, table: "Localizable.nocache")
        
    }
    
    func setLanguage(_ lang : String) {
        guard let bundleUrl = Bundle.main.url(forResource: "wovn", withExtension: "bundle") else {
            return
        }
        guard let wovnBundle = Bundle(url: bundleUrl) else {
            return
        }
        let path = wovnBundle.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
    }
}

