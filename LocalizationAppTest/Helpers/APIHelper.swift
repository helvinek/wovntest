//
//  APIHelper.swift
//  LocalizationAppTest
//
//  Created by Rafael Montilla on 1/15/19.
//  Copyright © 2019 RM. All rights reserved.
//

import UIKit
import Alamofire
import Files


class APIHelper {
    // MARK: - Properties
    
    static let shared = APIHelper()
    // Initialization
    private init() {}
    
    func fetchLocalizationData (_ completion : @escaping (_ success : Bool) -> ()){
        var urlString : String!
        //urlString = "https://my-json-server.typicode.com/RMontilla/LocalizationTest/db"
        urlString = "http://demo9863398.mockable.io/getLocalizations"
        guard let url = URL(string: urlString) else {
            completion(false)
            return
        }
        
        Alamofire.request(url, method: .get).validate().responseJSON { (response) in
            guard response.result.isSuccess else {
                print("error when fetching \(String(describing: response.result.error))")
                completion(false)
                return
            }
            
            guard let value = response.result.value as? NSDictionary else {
                print("Bad data \(String(describing: response.result.value))")
                    completion(false)
                    return
            }
            
            
            guard let languagesArray = value["languages"] as? [NSDictionary] else {
                completion(false)
                return
            }
            
            do {
                try self.updateFiles(languagesArray: languagesArray)
            } catch {
                print("error updateFiles")
            }
            
            completion(true)
        }
    }
    
    func  updateFiles(languagesArray : [NSDictionary]) throws{
        guard let bundleUrl = Bundle.main.url(forResource: "wovn", withExtension: "bundle") else {
            return
        }
        
        guard let wovnBundle = Bundle(url: bundleUrl) else {
            return
        }
        for langDict in languagesArray {
            let languageName = langDict["languageName"] as! String
            let paths = wovnBundle.paths(forResourcesOfType: "nocache.strings", inDirectory: "Resources", forLocalization: languageName)
            if paths.count > 0 {
                let filePath = paths[0]
                
                let file = try File(path:filePath)
                
                let valueArray = langDict["values"] as! [NSDictionary]
                for i in 0..<valueArray.count{
                    let valueDict = valueArray[i]
                    let keyStr = valueDict["key"] as! String
                    let valueStr = valueDict["value"] as! String
                    let entry = "\"\(keyStr)\" = \"\(valueStr)\";\n"
                    if i == 0 {
                        try file.write(string: entry)
                    } else {
                        try file.append(string: entry)
                    }
                }
            }
        }
    
    }
}


